# shmup

![](https://img.shields.io/badge/written%20in-C%20%28GBDK%29-blue)

A primitive vertical shooter for the Nintendo Game Boy.

- Four levels!
- Health!
- Managed to compile despite several bizarre bugs in basic functionality with the old version of `sdcc`!

Tags: game, homebrew


## Download

- [⬇️ game-r17.gb.zip](dist-archive/game-r17.gb.zip) *(11.73 KiB)*
- [⬇️ game-r17-src.zip](dist-archive/game-r17-src.zip) *(7.60 KiB)*
